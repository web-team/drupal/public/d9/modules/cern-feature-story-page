# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.0.4] - 10/12/2021

- Removed deprecated call `getCurrentUserId` from `core.base_field_override.node.feature_story_page.uid.yml` configuration
- New call is `default_value_callback: 'Drupal\node\Entity\Node::getDefaultEntityOwner'`

## [2.0.3] - 29/11/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer.

## [2.0.2] - 13/10/2021

- Add D9 readiness

## [2.0.1] - 28/05/2021

- Add composer support
- Add CHANGELOG
